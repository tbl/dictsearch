<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="ru_RU">
<context>
    <name>About</name>
    <message>
        <location filename="about.ui" line="14"/>
        <source>About</source>
        <translation>О программе</translation>
    </message>
    <message>
        <location filename="about.ui" line="53"/>
        <source>&amp;nbsp;&amp;nbsp;DictSearch. Version: %1&lt;br /&gt;
Copyright &amp;copy; 2012 Marat Buharov&lt;br /&gt;
&lt;br /&gt;
This application is designed to facilitate play &amp;quot;Hanging with friends&amp;quot;.</source>
        <oldsource>&amp;nbsp;&amp;nbsp;DictSearch&lt;br /&gt;
Copyright &amp;copy; 2012 Marat Buharov&lt;br /&gt;
&lt;br /&gt;
This application is designed to facilitate play &amp;quot;Hanging with friends&amp;quot;.</oldsource>
        <translation>&amp;nbsp;&amp;nbsp;DictSearch. Версия: %1&lt;br /&gt;
Copyright &amp;copy; 2012 Марат Бухаров&lt;br /&gt;
&lt;br /&gt;
Это приложение предназначено для облегчения игры в &amp;quot;Hanging with friends&amp;quot;.</translation>
    </message>
    <message>
        <source>DictSearch &amp;copy; 2012 Marat Buharov.&lt;br/&gt;
&lt;br/&gt;
This application is designed to facilitate play &amp;quot;Hanging with firends&amp;quot;.</source>
        <translation type="obsolete">DictSearch &amp;copy; 2012 Марат Бухаров.&lt;br/&gt;
&lt;br/&gt;
Это приложение предназначено для облегчения игры в &amp;quot;Hanging with friends&amp;quot;.</translation>
    </message>
</context>
<context>
    <name>MainWidget</name>
    <message>
        <location filename="mainwidget.ui" line="14"/>
        <source>Words Search</source>
        <translation>Поиск Слов</translation>
    </message>
    <message>
        <location filename="mainwidget.ui" line="28"/>
        <source>&amp;Words by letters</source>
        <translation>Слова по &amp;буквам</translation>
    </message>
    <message>
        <location filename="mainwidget.ui" line="36"/>
        <source>&amp;Letters:</source>
        <translation>Б&amp;уквы:</translation>
    </message>
    <message>
        <location filename="mainwidget.ui" line="49"/>
        <source>Multiplier:</source>
        <translation>Множитель:</translation>
    </message>
    <message>
        <location filename="mainwidget.ui" line="158"/>
        <source>Count:</source>
        <translation>Количество:</translation>
    </message>
    <message>
        <location filename="mainwidget.ui" line="211"/>
        <source>&amp;Clear</source>
        <translation>О&amp;чистить</translation>
    </message>
    <message>
        <location filename="mainwidget.ui" line="231"/>
        <source>&amp;Search</source>
        <translation>&amp;Поиск</translation>
    </message>
    <message>
        <location filename="mainwidget.ui" line="113"/>
        <source>Words by letter &amp;positions</source>
        <oldsource>Words by Letter &amp;Positions</oldsource>
        <translation>Слова по п&amp;озициям букв</translation>
    </message>
    <message>
        <location filename="mainwidget.ui" line="121"/>
        <source>P&amp;attern:</source>
        <oldsource>&amp;Pattern:</oldsource>
        <translation>&amp;Шаблон:</translation>
    </message>
    <message>
        <location filename="mainwidget.ui" line="134"/>
        <source>L&amp;etters not in word:</source>
        <translation>Буквы &amp;не в слове:</translation>
    </message>
    <message>
        <location filename="mainwidget.cpp" line="102"/>
        <source>Search in dictionary (%1 | %2)</source>
        <translation>Поиск в словаре (%1 | %2)</translation>
    </message>
    <message>
        <location filename="mainwidget.cpp" line="103"/>
        <source>Clear fields (%1)</source>
        <translation>Очистить поля (%1)</translation>
    </message>
    <message>
        <location filename="mainwidget.cpp" line="129"/>
        <source>Common</source>
        <translation>Общий</translation>
    </message>
    <message>
        <location filename="mainwidget.cpp" line="130"/>
        <source>Hanging with friends</source>
        <translation>Hanging with friends</translation>
    </message>
    <message>
        <location filename="mainwidget.cpp" line="296"/>
        <source>Dictionary</source>
        <translation>Словарь</translation>
    </message>
    <message>
        <location filename="mainwidget.cpp" line="301"/>
        <source>About</source>
        <translation>О программе</translation>
    </message>
    <message>
        <location filename="mainwidget.cpp" line="302"/>
        <source>About Qt</source>
        <translation>О Qt</translation>
    </message>
</context>
<context>
    <name>ScoreTableColumnChar</name>
    <message>
        <source>Letter</source>
        <translation type="obsolete">Буква</translation>
    </message>
</context>
<context>
    <name>ScoreTableColumnCount</name>
    <message>
        <source>Count</source>
        <translation type="obsolete">Количество</translation>
    </message>
</context>
<context>
    <name>ScoreTableColumnScore</name>
    <message>
        <location filename="scoretablecolumnscore.h" line="32"/>
        <source>Score</source>
        <translation>Очки</translation>
    </message>
</context>
<context>
    <name>ScoreTableColumnWord</name>
    <message>
        <location filename="scoretablecolumnword.h" line="31"/>
        <source>Word</source>
        <translation>Слово</translation>
    </message>
</context>
<context>
    <name>ScoreTableModel</name>
    <message>
        <source>Word</source>
        <translation type="obsolete">Слово</translation>
    </message>
    <message>
        <source>Score</source>
        <translation type="obsolete">Очки</translation>
    </message>
</context>
<context>
    <name>Splash</name>
    <message>
        <location filename="splash.cpp" line="25"/>
        <source>Loading...</source>
        <oldsource>Dict loading...</oldsource>
        <translation>Загрузка...</translation>
    </message>
    <message>
        <location filename="splash.cpp" line="31"/>
        <source>Finish</source>
        <translation>Закончили</translation>
    </message>
    <message>
        <location filename="splash.h" line="33"/>
        <source>Words loaded: %1</source>
        <translation>Слов загружено: %1</translation>
    </message>
    <message>
        <location filename="splash.h" line="34"/>
        <source>Almost finish</source>
        <oldsource>Finish</oldsource>
        <translation>Почти закончили</translation>
    </message>
</context>
<context>
    <name>StatTableColumnChar</name>
    <message>
        <location filename="stattablecolumnchar.h" line="31"/>
        <source>Letter</source>
        <translation>Буква</translation>
    </message>
</context>
<context>
    <name>StatTableColumnCount</name>
    <message>
        <location filename="stattablecolumncount.h" line="32"/>
        <source>Count</source>
        <translation>Количество</translation>
    </message>
</context>
<context>
    <name>StatTableColumnScore</name>
    <message>
        <location filename="stattablecolumnscore.h" line="32"/>
        <source>Score</source>
        <translation>Очки</translation>
    </message>
</context>
<context>
    <name>StatTableModel</name>
    <message>
        <source>Letter</source>
        <translation type="obsolete">Буква</translation>
    </message>
    <message>
        <source>Score</source>
        <translation type="obsolete">Очки</translation>
    </message>
    <message>
        <source>Count</source>
        <translation type="obsolete">Количество</translation>
    </message>
</context>
</TS>
