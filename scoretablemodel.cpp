/*
  DictSearch
Copyright (C) 2012 Marat Buharov

This file is part of DictSearch.

DictSearch is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, version 3 of the License.

DictSearch is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with DictSearch.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "scoretablemodel.h"
#include "dictionary.h"
#include <QHash>

ScoreTableModel::ScoreTableModel(Dictionary *dict)
    : QAbstractTableModel(dict),
      mScores(), mDict(*dict), mLetters(), mLettersNW(), stcf(new ScoreTableColumnFactory(this)) {
}

QVariant ScoreTableModel::data(const QModelIndex &index, int role) const {
    int row = index.row();
    int col = index.column();

    return role == Qt::DisplayRole
            && row >= 0
            && index.isValid()
            && row < mScores.size()
            && stcf->contains(col)
            ? stcf->cm(col)->data(mScores.at(row))
            : QVariant();
}

QVariant ScoreTableModel::headerData(int section, Qt::Orientation orientation, int role) const {
    return role == Qt::DisplayRole
            && orientation == Qt::Horizontal
            && stcf->contains(section)
            ? stcf->cm(section)->headerData()
            : QVariant();
}

void ScoreTableModel::sort(int column, Qt::SortOrder order) {
    if(!stcf->contains(column))
        return;

    emit layoutAboutToBeChanged();
    stcf->cm(column)->order(mScores, order);
    emit layoutChanged();
}

void ScoreTableModel::searchWordsByLetters(const QString &str, int multPos, const CostModifierBase *cm) {
    int oldSize = mScores.size();
    beginResetModel();
    mLetters = str;
    mScores = mDict.getWordsByLetters(str, 200, 4, 8, multPos, cm);
    endResetModel();
    emit newData();
    int newSize = mScores.size();
    if(newSize != oldSize)
        emit sizeChanged(newSize);
}

void ScoreTableModel::searchWordsByLettersPos(const QString &str, const QString &strNW) {
    int oldSize = mScores.size();
    beginResetModel();
    mLetters = str;
    mLettersNW = strNW;
    mScores = mDict.getWordsByPosLetters(str, strNW);
    endResetModel();
    emit newData();
    int newSize = mScores.size();
    if(newSize != oldSize)
        emit sizeChanged(newSize);
}

void ScoreTableModel::clear() {
    emit emptyData();
    mLetters.clear();
    mLettersNW.clear();
    if(mScores.isEmpty())
        return;
    beginResetModel();
    mScores.clear();
    endResetModel();
    emit sizeChanged(0);
}
