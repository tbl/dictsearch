/*
  DictSearch
Copyright (C) 2012 Marat Buharov

This file is part of DictSearch.

DictSearch is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, version 3 of the License.

DictSearch is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with DictSearch.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef STATTABLECOLUMNFACTORY_H
#define STATTABLECOLUMNFACTORY_H

#include <QObject>
#include <QHash>
#include "stattablecolumn.h"

class StatTableColumnFactory : public QObject {
    Q_OBJECT
public:
    explicit StatTableColumnFactory(QObject *parent = 0);
    const StatTableColumnBase *cm(int id) const Q_REQUIRED_RESULT { return cont.value(id, 0); }
    bool contains(int id) const Q_REQUIRED_RESULT { return cont.contains(id); }
    int getMaxCol() const Q_REQUIRED_RESULT { return maxCol + 1; }
    ~StatTableColumnFactory();

private:
    template<int ID>
    void insert(const StatTableColumn<ID> *c);
    typedef QHash<int, const StatTableColumnBase *> t_cont;
    t_cont cont;
    int maxCol;

    Q_DISABLE_COPY(StatTableColumnFactory)
};

#endif // STATTABLECOLUMNFACTORY_H
