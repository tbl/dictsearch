/*
  DictSearch
Copyright (C) 2012 Marat Buharov

This file is part of DictSearch.

DictSearch is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, version 3 of the License.

DictSearch is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with DictSearch.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef COSTMODIFIERFACTORY_H
#define COSTMODIFIERFACTORY_H

#include <QObject>
#include "costmodifier.h"
#include <QHash>

class CostModifierFactory : public QObject {
    Q_OBJECT

public:
    explicit CostModifierFactory(QObject *parent = 0);
    const CostModifierBase *cm(CostModifierBase::t_id id) const Q_REQUIRED_RESULT { return cont.value(id, cont.value(CostModifierBase::IDENT)); }
    int size() const Q_REQUIRED_RESULT { return cont.size(); }
    ~CostModifierFactory();

private:
    template<CostModifierBase::t_id ID>
    void insert(const CostModifier<ID> *c) { cont.insert(ID, c); }
    typedef QHash<CostModifierBase::t_id, const CostModifierBase *> t_cont;
    t_cont cont;

    Q_DISABLE_COPY(CostModifierFactory)
};

#endif // COSTMODIFIERFACTORY_H
