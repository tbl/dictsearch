/*
  DictSearch
Copyright (C) 2012 Marat Buharov

This file is part of DictSearch.

DictSearch is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, version 3 of the License.

DictSearch is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with DictSearch.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "columnutility.h"
#include "dictionary.h"

template<>
bool comparator(const QChar &t1, const QChar &t2) {
    QChar tt1 = Dictionary::alphabetize(t1);
    QChar tt2 = Dictionary::alphabetize(t2);
    return tt1 == tt2 ? t1 > t2 : tt1 > tt2;
}

