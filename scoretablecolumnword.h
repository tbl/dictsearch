/*
  DictSearch
Copyright (C) 2012 Marat Buharov

This file is part of DictSearch.

DictSearch is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, version 3 of the License.

DictSearch is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with DictSearch.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef SCORETABLECOLUMNWORD_H
#define SCORETABLECOLUMNWORD_H

#include "scoretablecolumn.h"
#include <QCoreApplication>

enum { Word = 0 };

template<>
class ScoreTableColumn<Word> : public ScoreTableColumnBase {
public:
    ScoreTableColumn<Word>() : ScoreTableColumnBase(QCoreApplication::translate("ScoreTableColumnWord", "Word")) {}
    QString data(const QPair<QString, int> &p) const Q_REQUIRED_RESULT { return p.first; }
    void order(QList<QPair<QString, int> > &l, Qt::SortOrder order) const;
};

#endif // SCORETABLECOLUMNWORD_H
