/*
  DictSearch
Copyright (C) 2012 Marat Buharov

This file is part of DictSearch.

DictSearch is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, version 3 of the License.

DictSearch is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with DictSearch.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef COSTMODIFIERIDENT_H
#define COSTMODIFIERIDENT_H

#include "costmodifier.h"

template<>
class CostModifier<CostModifierBase::IDENT> : public CostModifierBase {
public:
    CostModifier<CostModifierBase::IDENT>() : CostModifierBase(QLatin1String("__"), QLatin1String("IDENT")) {}
    virtual int calcCost(int, int wordCost) const Q_REQUIRED_RESULT { return wordCost; }
};

#endif // COSTMODIFIERIDENT_H
