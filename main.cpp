/*
  DictSearch
Copyright (C) 2012 Marat Buharov

This file is part of DictSearch.

DictSearch is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, version 3 of the License.

DictSearch is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with DictSearch.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <QApplication>
#include <QTranslator>
#include <QLocale>
#include "mainwidget.h"
#include <QLibraryInfo>

int main(int argc, char *argv[]) {
    QApplication a(argc, argv);

    QTranslator translator;
    QTranslator qtTranslator;

    translator.load(QLocale::system(), QLatin1String("dictsearch"),
                    QLatin1String("_"), QLatin1String(":/translations"));
    qtTranslator.load(QLocale::system(), QLatin1String("qt"),
                      QLatin1String("_"), QLibraryInfo::location(QLibraryInfo::TranslationsPath));

    a.installTranslator(&translator);
    a.installTranslator(&qtTranslator);

    MainWidget w;
    w.show();
    
    return a.exec();
}
