/*
  DictSearch
Copyright (C) 2012 Marat Buharov

This file is part of DictSearch.

DictSearch is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, version 3 of the License.

DictSearch is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with DictSearch.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef STATTABLEMODEL_H
#define STATTABLEMODEL_H

#include <QAbstractTableModel>
#include <QList>
#include "stattablecolumnfactory.h"

class ScoreTableModel;

class StatTableModel : public QAbstractTableModel {
    Q_OBJECT
public:
    explicit StatTableModel(ScoreTableModel *parentModel);
    int rowCount(const QModelIndex &parent = QModelIndex()) const Q_REQUIRED_RESULT { return parent.isValid() ? 0 : mLetterStat.size(); }
    int columnCount(const QModelIndex &parent = QModelIndex()) const Q_REQUIRED_RESULT { return parent.isValid() ? 0 : stcf->getMaxCol(); }
    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const Q_REQUIRED_RESULT;
    QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const Q_REQUIRED_RESULT;
    void sort(int column, Qt::SortOrder order = Qt::AscendingOrder);
    Qt::ItemFlags flags(const QModelIndex &index) const Q_REQUIRED_RESULT { return index.isValid() ? QAbstractTableModel::flags(index) : Qt::ItemIsEnabled; }
    
public slots:
    void clear();

private slots:
    void refreshModel();

private:
    typedef QPair<QChar, int> t_letter_pair;
    typedef QList<t_letter_pair> t_letter_cont;
    t_letter_cont mLetterStat;
    const ScoreTableModel &pModel;
    const StatTableColumnFactory *stcf;

    Q_DISABLE_COPY(StatTableModel)
};

#endif // STATTABLEMODEL_H
