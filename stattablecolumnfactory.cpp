/*
  DictSearch
Copyright (C) 2012 Marat Buharov

This file is part of DictSearch.

DictSearch is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, version 3 of the License.

DictSearch is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with DictSearch.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "stattablecolumnfactory.h"
#include "stattablecolumnchar.h"
#include "stattablecolumnscore.h"
#include "stattablecolumncount.h"

StatTableColumnFactory::StatTableColumnFactory(QObject *parent) :
    QObject(parent) {
    insert(new StatTableColumn<Char>());
    insert(new StatTableColumn<Score>());
    insert(new StatTableColumn<Count>());
}

StatTableColumnFactory::~StatTableColumnFactory() {
    foreach(const StatTableColumnBase *cm, cont)
        delete cm;
}

template<int ID>
void StatTableColumnFactory::insert(const StatTableColumn<ID> *c) {
    cont.insert(ID, c);
    maxCol = qMax(maxCol, ID);
}
