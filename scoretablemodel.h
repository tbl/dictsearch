/*
  DictSearch
Copyright (C) 2012 Marat Buharov

This file is part of DictSearch.

DictSearch is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, version 3 of the License.

DictSearch is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with DictSearch.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef SCORETABLEMODEL_H
#define SCORETABLEMODEL_H

#include <QAbstractTableModel>
#include "scoretablecolumnfactory.h"

class Dictionary;
class CostModifierBase;

class ScoreTableModel : public QAbstractTableModel {
    Q_OBJECT
public:
    typedef QPair<QString, int> t_score;
    typedef QList<t_score> t_scores;
    explicit ScoreTableModel(Dictionary *dict);
    int rowCount(const QModelIndex &parent = QModelIndex()) const Q_REQUIRED_RESULT { return parent.isValid() ? 0 : mScores.size(); }
    int columnCount(const QModelIndex &parent = QModelIndex()) const Q_REQUIRED_RESULT { return parent.isValid() ? 0 : stcf->getMaxCol(); }
    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const Q_REQUIRED_RESULT;
    QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const Q_REQUIRED_RESULT;
    Qt::ItemFlags flags(const QModelIndex &index) const Q_REQUIRED_RESULT { return index.isValid() ? QAbstractTableModel::flags(index) : Qt::ItemIsEnabled; }
    void sort(int column, Qt::SortOrder order = Qt::AscendingOrder);
    const t_scores &scores() const Q_REQUIRED_RESULT { return mScores; }
    const QString &letters() const Q_REQUIRED_RESULT { return mLetters; }
    const QString &lettersNW() const Q_REQUIRED_RESULT { return mLettersNW; }

public slots:
    void searchWordsByLetters(const QString &str, int multPos, const CostModifierBase *cm);
    void searchWordsByLettersPos(const QString &str, const QString &strNW);
    void clear();
    
signals:
    void sizeChanged(int);
    void newData();
    void emptyData();

private:
    t_scores mScores;
    const Dictionary &mDict;
    QString mLetters;
    QString mLettersNW;
    const ScoreTableColumnFactory *stcf;

    Q_DISABLE_COPY(ScoreTableModel)
};

#endif // SCORETABLEMODEL_H
