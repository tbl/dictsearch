/*
  DictSearch
Copyright (C) 2012 Marat Buharov

This file is part of DictSearch.

DictSearch is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, version 3 of the License.

DictSearch is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with DictSearch.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef INTLINEEDIT_H
#define INTLINEEDIT_H

#include <QLineEdit>
#include "dictionary.h"

class IntLineEdit : public QLineEdit {
    Q_OBJECT
public:
    explicit IntLineEdit(QWidget *parent) : QLineEdit(Dictionary::formatInt(0), parent) {}
    explicit IntLineEdit(int i = 0, QWidget* parent = 0) : QLineEdit(Dictionary::formatInt(i), parent) {}
    
public slots:
    void setInt(int i) { setText(Dictionary::formatInt(i)); }

private:
    Q_DISABLE_COPY(IntLineEdit)
};

#endif // INTLINEEDIT_H
