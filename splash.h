/*
  DictSearch
Copyright (C) 2012 Marat Buharov

This file is part of DictSearch.

DictSearch is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, version 3 of the License.

DictSearch is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with DictSearch.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef SPLASH_H
#define SPLASH_H

#include <QSplashScreen>

class Splash : public QSplashScreen {
    Q_OBJECT
    
public:
    explicit Splash(QWidget *main);

public slots:
    void loadReady();
    void setWordCount(int cnt) { showMessage(tr("Words loaded: %1").arg(cnt), msgAlign); }
    void almostFinish() { showMessage(tr("Almost finish"), msgAlign); }

private:
    QWidget *pMain;
    static const Qt::Alignment msgAlign;
};

#endif // SPLASH_H
