/*
  DictSearch
Copyright (C) 2012 Marat Buharov

This file is part of DictSearch.

DictSearch is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, version 3 of the License.

DictSearch is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with DictSearch.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "stattablecolumnchar.h"
#include "columnutility.h"

namespace {

template<Qt::SortOrder>
class CharComparer;

template<>
class CharComparer<Qt::DescendingOrder> : public CompareNS::Comparer<QPair<QChar, int>, Qt::DescendingOrder> {
public:
    bool operator()(const QPair<QChar, int> &t1, const QPair<QChar, int> &t2) const Q_REQUIRED_RESULT {
        return comparator(t1.first, t2.first);
    }
};

template<>
class CharComparer<Qt::AscendingOrder> : public CompareNS::Comparer<QPair<QChar, int>, Qt::AscendingOrder> {
public:
    bool operator()(const QPair<QChar, int> &t1, const QPair<QChar, int> &t2) const Q_REQUIRED_RESULT {
        return comparator(t2.first, t1.first);
    }
};

CompareNS::ComparerSelector<QPair<QChar, int>, CharComparer<Qt::AscendingOrder>, CharComparer<Qt::DescendingOrder> > ccs;

}

void StatTableColumn<Char>::order(QList<QPair<QChar, int> > &l, Qt::SortOrder order) const {
    orderer(l, ccs, order);
}
