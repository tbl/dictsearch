/*
  DictSearch
Copyright (C) 2012 Marat Buharov

This file is part of DictSearch.

DictSearch is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, version 3 of the License.

DictSearch is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with DictSearch.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef COSTMODIFIER_H
#define COSTMODIFIER_H

#include <QString>

class CostModifierBase {
    const QString str;
    const QString eStr;

protected:
    CostModifierBase(const QString &str, const QString &eStr) : str(str), eStr(eStr) {}
    CostModifierBase(const QString &str) : str(str), eStr(str) {}

public:
    enum t_id { IDENT = 0, DL, TL, DW, TW };

    operator const QString &() const Q_REQUIRED_RESULT { return str; }
    const QString &enumStr() const Q_REQUIRED_RESULT { return eStr; }
    virtual int calcCost(int letterCost, int wordCost) const Q_REQUIRED_RESULT = 0;
    virtual ~CostModifierBase() {}
};


template<CostModifierBase::t_id ID>
class CostModifier;

#endif // COSTMODIFIER_H
