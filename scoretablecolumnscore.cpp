/*
  DictSearch
Copyright (C) 2012 Marat Buharov

This file is part of DictSearch.

DictSearch is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, version 3 of the License.

DictSearch is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with DictSearch.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "scoretablecolumnscore.h"
#include "columnutility.h"

namespace {

template<Qt::SortOrder>
class ScoreComparer;

template<>
class ScoreComparer<Qt::DescendingOrder> : public CompareNS::Comparer<QPair<QString, int>, Qt::DescendingOrder> {
public:
    bool operator()(const QPair<QString, int> &t1, const QPair<QString, int> &t2) const Q_REQUIRED_RESULT {
        return comparator(t1.second, t2.second,
                          t1.first.size(), t2.first.size(),
                          t2.first, t1.first);
    }
};

template<>
class ScoreComparer<Qt::AscendingOrder> : public CompareNS::Comparer<QPair<QString, int>, Qt::AscendingOrder> {
public:
    bool operator()(const QPair<QString, int> &t1, const QPair<QString, int> &t2) const Q_REQUIRED_RESULT {
        return comparator(t2.second, t1.second,
                          t2.first.size(), t1.first.size(),
                          t2.first, t1.first);
    }
};

CompareNS::ComparerSelector<QPair<QString, int>, ScoreComparer<Qt::AscendingOrder>, ScoreComparer<Qt::DescendingOrder> > scs;

}

void ScoreTableColumn<Score>::order(QList<QPair<QString, int> > &l, Qt::SortOrder order) const {
    orderer(l, scs, order);
}
