/*
  DictSearch
Copyright (C) 2012 Marat Buharov

This file is part of DictSearch.

DictSearch is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, version 3 of the License.

DictSearch is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with DictSearch.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef UPPERVALIDATOR_H
#define UPPERVALIDATOR_H

#include <QValidator>

class UpperValidator : public QValidator {
    Q_OBJECT
public:
    explicit UpperValidator(QObject *parent = 0) : QValidator(parent) {}
    State validate(QString &input, int &) const Q_REQUIRED_RESULT;

private:
    Q_DISABLE_COPY(UpperValidator)
};

#endif // UPPERVALIDATOR_H
