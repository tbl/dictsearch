/*
  DictSearch
Copyright (C) 2012 Marat Buharov

This file is part of DictSearch.

DictSearch is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, version 3 of the License.

DictSearch is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with DictSearch.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "dictionary.h"
#include <QFile>
#include <QTextStream>
#include <QQueue>
#include <QSet>
#include <QPair>
#include <QStringBuilder>
#include <numeric>
#include <QtConcurrentRun>
#include <QFuture>
#include <QFutureWatcher>
#include "aggregatewatcher.h"
#include "costmodifier.h"

namespace {

typedef QSet<QString> t_uniq_container;
typedef QSet<QPair<QChar, int> > t_uniq_letter_pos;
typedef QSet<QChar> t_uniq_letter;

template<class T> class Enqueuer;
template<class E> class SetAppender;
template<class T, class Y> class StatSummator;
template<class E, class Y> class ContStatSummator;
template<class E, class Y> class ContUniqStatSummator;

QString removeNonLetters(const QString &str) Q_REQUIRED_RESULT;

template<class T>
T sort(const T &str) Q_REQUIRED_RESULT;

t_uniq_container removeChar(const QString &str, int minWordLen) Q_REQUIRED_RESULT;

template<class T>
Enqueuer<T> makeEnqueuer(QQueue<T> &x, QSet<T> &y) Q_REQUIRED_RESULT;

template<class E>
SetAppender<E> makeSetAppender(QSet<E> &s) Q_REQUIRED_RESULT;

bool hasNonLetters(const QString &str) Q_REQUIRED_RESULT;

t_uniq_container loadDict(QString file) Q_REQUIRED_RESULT;

int calcCost(const QString &str, int multPos = -1, const CostModifierBase *cm = 0) Q_REQUIRED_RESULT;

template<class T, class Y>
StatSummator<T, Y> makeStatSummator(QHash<T, Y> &s) Q_REQUIRED_RESULT;

template<class E, class Y>
ContStatSummator<E, Y> makeContStatSummator(QHash<E, Y> &s) Q_REQUIRED_RESULT;

template<class T>
QSet<typename T::value_type> distincter(const T &str) Q_REQUIRED_RESULT;

template<class E, class Y>
ContUniqStatSummator<E, Y> makeContUniqStatSummator(QHash<E, Y> &s) Q_REQUIRED_RESULT;

class InitVowels {
public:
    InitVowels() {
        vowels << QLatin1Char('A');
        vowels << QLatin1Char('E');
        vowels << QLatin1Char('I');
        vowels << QLatin1Char('O');
        vowels << QLatin1Char('U');
    }
    t_uniq_letter vowels;
};

template<class T>
class Enqueuer {
    QQueue<T> &q;
    QSet<T> &s;

public:
    Enqueuer(QQueue<T> &qq, QSet<T> &ss) : q(qq), s(ss) {}

    void operator()(const T &element) {
        if(!s.contains(element)) {
            q.enqueue(element);
            s << element;
        }
    }
};

template<class E>
class SetAppender {
    QSet<E> &s;

public:
    SetAppender(QSet<E> &ss) : s(ss) {}
    void operator()(const E &e) { s << e; }
};

class IsNonLetter {
public:
    bool operator()(QChar ch) const Q_REQUIRED_RESULT { return !ch.isLetter(); }
};

template<class T, class Y>
class StatSummator {
    QHash<T, Y> &s;

public:
    StatSummator(QHash<T, Y> &ss) : s(ss) {}

    void operator()(const T &element) {
        typename QHash<T, Y>::iterator it = s.find(element);
        if(it == s.end())
            it = s.insert(element, Y());
        ++it.value();
    }
};

template<class E, class Y>
class ContStatSummator {
    QHash<E, Y> &s;

public:
    ContStatSummator(QHash<E, Y> &ss) : s(ss) {}

    template<class T>
    void operator()(const T &cont) {
        const T &alphaCont = Dictionary::alphabetize(cont);
        std::for_each(alphaCont.constBegin(), alphaCont.constEnd(), makeStatSummator(s));
    }
};

template<class E, class Y>
class ContUniqStatSummator {
    QHash<E, Y> &s;

public:
    ContUniqStatSummator(QHash<E, Y> &ss) : s(ss) {}

    template<class T>
    void operator()(const T &cont) {
        const QSet<E> &uniq = distincter(Dictionary::alphabetize(cont));
        std::for_each(uniq.constBegin(), uniq.constEnd(), makeStatSummator(s));
    }
};

const t_uniq_letter vowels = InitVowels().vowels;
const QLatin1String codecName("ISO 8859-15");

QString removeNonLetters(const QString &str) {
    QString result(str.size(), QChar());
    int cnt = 0;

    foreach(QChar ch, str)
        if(ch.isLetter())
            result[cnt++] = ch;

    result.truncate(cnt);

    return result;
}

template<class T>
T sort(const T &str) {
    T sortedStr = str;
    qSort(sortedStr.begin(), sortedStr.end());

    return sortedStr;
}

t_uniq_container removeChar(const QString &str, int minWordLen) {
    t_uniq_container container;
    const int str_size = str.size();

    if(str_size > minWordLen)
        for(int i = 0; i < str_size; ++i)
            container << QString(str).remove(i, 1);

    return container;
}

template<class T>
Enqueuer<T> makeEnqueuer(QQueue<T> &x, QSet<T> &y) {
    return Enqueuer<T>(x, y);
}

template<class E>
SetAppender<E> makeSetAppender(QSet<E> &s) {
    return SetAppender<E>(s);
}

bool hasNonLetters(const QString &str) {
    return std::find_if(str.constBegin(), str.constEnd(), IsNonLetter()) != str.constEnd();
}

t_uniq_container loadDict(QString file) {
    t_uniq_container container;
    QFile dict_file(file);

    if(!dict_file.open(QFile::ReadOnly))
        return container;

    QTextStream dict_stream(&dict_file);
    dict_stream.setCodec(codecName.latin1());

    while(!dict_stream.atEnd()) {
        const QString &dict_line = dict_stream.readLine().simplified().toUpper();
        if(!dict_line.isEmpty() && !hasNonLetters(dict_line))
            container << dict_line;
    }

    dict_file.close();

    return container;
}

int calcCost(const QString &str, int multPos, const CostModifierBase *cm) {
    int str_size = str.size();
    int result = 0;
    for(int i = 0; i < str_size; ++i)
        result += Dictionary::calcCost(str[i]);
    if(multPos >= 0 && multPos < str_size && cm != 0)
        return cm->calcCost(Dictionary::calcCost(str[multPos]), result);
    return result;
}

template<class T, class Y>
StatSummator<T, Y> makeStatSummator(QHash<T, Y> &s) {
    return StatSummator<T, Y>(s);
}

template<class E, class Y>
ContStatSummator<E, Y> makeContStatSummator(QHash<E, Y> &s) {
    return ContStatSummator<E, Y>(s);
}

template<class T>
QSet<typename T::value_type> distincter(const T &str) {
    QSet<typename T::value_type> result;
    std::for_each(str.constBegin(), str.constEnd(), makeSetAppender(result));
    return result;
}

template<class E, class Y>
ContUniqStatSummator<E, Y> makeContUniqStatSummator(QHash<E, Y> &s) {
    return ContUniqStatSummator<E, Y>(s);
}

const char charCost[26] = {
    1, 4, 4, 2, 1, 4, //A-F
    3, 3, 1, 10, 5, 2, //G-L
    4, 2, 1, 4, 10, 1, //M-R
    1, 1, 2, 5, 4, 8, //S-X
    3, 10 //Y-Z
};

}

void Dictionary::clearDict() {
    clearIndex();
    dict.clear();
}

void Dictionary::clearIndex() {
    index.clear();
    sortedDict.clear();
    letterInWord.clear();
    lengthMap.clear();
}

void Dictionary::load(const QString &path) {
    clearDict();

    QFile dict_filenames(path % QLatin1Literal("filenames"));

    if(!dict_filenames.open(QFile::ReadOnly))
        return;

    QTextStream filenames_stream(&dict_filenames);
    filenames_stream.setCodec(codecName.latin1());

    AggregateWatcher *aggWatcher = new AggregateWatcher(this);
    connect(aggWatcher, SIGNAL(noMoreWatchers()), SLOT(loadAllFinished()));

    while(!filenames_stream.atEnd()) {
        const QString &dict_name = filenames_stream.readLine();
        if(!dict_name.isEmpty()) {
            const QString &dict_file_name = path % dict_name;
            QFutureWatcher<t_uniq_container> *fw = new QFutureWatcher<t_uniq_container>(aggWatcher);
            connect(fw, SIGNAL(finished()), SLOT(loadFinished()));
            QFuture<t_uniq_container> future = QtConcurrent::run(loadDict, dict_file_name);
            fw->setFuture(future);
        }
    }

    dict_filenames.close();
}

void Dictionary::loadAllFinished() {
    AggregateWatcher *aggWatcher = qobject_cast<AggregateWatcher *>(sender());
    if(aggWatcher != 0)
        aggWatcher->deleteLater();

    emit preCopyDict();

    dict = uniqDict.toList();
    uniqDict.clear();

    emit dictReady();
}

void Dictionary::loadFinished() {
    QFutureWatcher<t_uniq_container> *fw = static_cast<QFutureWatcher<t_uniq_container> *>(qobject_cast<QFutureWatcherBase *>(sender()));
    if(fw == 0)
        return;
    uniqDict |= fw->future();
    fw->deleteLater();
    emit wordCountChanged(uniqDict.size());
}

Dictionary::t_scores Dictionary::getWordsByLetters(
        const QString &letters, int maxCount, int minWordLen, int maxWordLen, int multPos, const CostModifierBase *cm) const {
    t_scores result;
    if(maxCount < 1 || letters.isEmpty() || sortedDict.isEmpty())
        return result;

    const QString &alphaStr = sort(alphabetize(letters));
    int lastLen = alphaStr.size();

    typedef QQueue<QString> t_tree_queue;

    t_tree_queue levelQ;
    t_uniq_container strSearched;

    levelQ.enqueue(alphaStr);

    Enqueuer<QString> enqueuer(levelQ, strSearched);

    while(!levelQ.isEmpty()) {
        const QString &chars = levelQ.dequeue();
        int charsLen = chars.size();

        if(charsLen < lastLen) {
            lastLen = charsLen;
            strSearched.clear();
        }

        if(maxWordLen <= 0 || charsLen <= maxWordLen)
            for(t_sorted_cont::const_iterator it = sortedDict.constFind(chars);
                it != sortedDict.constEnd() && it.key() == chars;
                ++it) {
                const QString &word = dict.at(it.value());
                result << qMakePair(word, ::calcCost(word, multPos, cm));
                if(result.size() >= maxCount)
                    return result;
            }

        const t_uniq_container &croppedStr = removeChar(chars, minWordLen);

        std::for_each(croppedStr.constBegin(), croppedStr.constEnd(), enqueuer);
    }

    return result;
}

Dictionary::t_scores Dictionary::getWordsByPosLetters(
        const QString &letters, const QString &notInWord,
        int maxCount, bool uniqOpen, bool noVowelsAtLast) const {
    t_scores result;
    if(letters.isEmpty() || index.isEmpty())
        return result;

    const QString &alphaStr = alphabetize(letters);
    const QString &niwAlphaStr = alphabetize(notInWord);
    const int str_len = alphaStr.size();
    const int niwAlphaStrLen = niwAlphaStr.size();

    t_bitmap::const_iterator lit = lengthMap.constFind(str_len);
    if(lit == lengthMap.constEnd())
        return result;

    QBitArray resultArr = lit.value();

    t_uniq_letter uniqLetters;
    t_uniq_letter_pos uniqLettersPos;

    for(int i_letter = 0; i_letter < str_len; ++i_letter) {
        QChar letter = alphaStr.at(i_letter);
        if(!letter.isLetter())
            continue;

        uniqLettersPos << qMakePair(letter, i_letter);
        uniqLetters << letter;

        t_bitindex::const_iterator it = index.constFind(letter);
        if(it == index.constEnd())
            return result;
        const t_bitmap &letterMap = it.value();
        t_bitmap::const_iterator lmit = letterMap.constFind(i_letter);
        if(lmit == letterMap.constEnd())
            return result;
        resultArr &= lmit.value();
    }

    for(int i_niw = 0; i_niw < niwAlphaStrLen; ++i_niw) {
        t_letter_map::const_iterator niwit = letterInWord.constFind(niwAlphaStr.at(i_niw));
        if(niwit != letterInWord.constEnd())
            resultArr &= ~niwit.value();
    }

    if(noVowelsAtLast) {
        t_uniq_letter remain_vowels = vowels;

        int lastVowelPos = -1;
        for(int i_letter = 0; i_letter < str_len; ++i_letter) {
            QChar ch = alphaStr.at(i_letter);
            if(vowels.contains(ch)) {
                remain_vowels.remove(ch);
                lastVowelPos = i_letter;
            }
        }

        for(int i_niw = 0; i_niw < niwAlphaStrLen; ++i_niw)
            remain_vowels.remove(niwAlphaStr.at(i_niw));

        for(t_uniq_letter::const_iterator it = remain_vowels.constBegin(); it != remain_vowels.constEnd(); ++it) {
            t_bitindex::const_iterator bit = index.constFind(*it);
            if(bit == index.constEnd())
                continue;
            const t_bitmap &vowMap = bit.value();
            for(int i_vows = lastVowelPos + 1; i_vows < str_len; ++i_vows) {
                if(alphaStr.at(i_vows).isLetter())
                    continue;
                t_bitmap::const_iterator vowit = vowMap.constFind(i_vows);
                if(vowit == vowMap.constEnd())
                    continue;
                resultArr &= ~vowit.value();
            }
        }
    }


    if(uniqOpen)
        foreach(QChar letter, uniqLetters) {
            t_bitindex::const_iterator it = index.constFind(letter);
            if(it == index.constEnd())
                continue;
            const t_bitmap &letterMap = it.value();
            for(int i_letter = 0; i_letter < str_len; ++i_letter)
                if(!uniqLettersPos.contains(qMakePair(letter, i_letter))) {
                    t_bitmap::const_iterator lmit = letterMap.constFind(i_letter);
                    if(lmit != letterMap.constEnd())
                        resultArr &= ~lmit.value();
                }
        }

    const int dict_size = dict.size();

    for(int i = 0; i < dict_size; ++i)
        if(resultArr.testBit(i)) {
            const QString &word = dict.at(i);
            result << qMakePair(word, ::calcCost(word));
            if(maxCount != 0 && result.size() >= maxCount)
                break;
        }

    return result;
}

void Dictionary::makeIndex() {
    clearIndex();

    const int dict_size = dict.size();
    QBitArray emptyBitArray(dict_size);

    for(int i_word = 0; i_word < dict_size; ++i_word) {

        const QString &alphaStr = alphabetize(removeNonLetters(dict.at(i_word)));
        const int word_size = alphaStr.size();

        sortedDict.insert(sort(alphaStr), i_word);
        t_bitmap::iterator lit = lengthMap.find(word_size);
        if(lit == lengthMap.end())
            lit = lengthMap.insert(word_size, emptyBitArray);
        lit.value().setBit(i_word);

        for(int i_letter = 0; i_letter < word_size; ++i_letter) {
            QChar letter = alphaStr.at(i_letter);
            t_bitindex::iterator it = index.find(letter);
            if(it == index.end())
                it = index.insert(letter, t_bitmap());
            t_bitmap &letterMap = it.value();
            t_bitmap::iterator lmit = letterMap.find(i_letter);
            if(lmit == letterMap.end())
                lmit = letterMap.insert(i_letter, emptyBitArray);
            lmit.value().setBit(i_word);

            t_letter_map::iterator liwit = letterInWord.find(letter);
            if(liwit == letterInWord.end())
                liwit = letterInWord.insert(letter, emptyBitArray);
            liwit.value().setBit(i_word);
        }
    }

    emit indexReady();
}

Dictionary::t_letter_stat Dictionary::letterStats(const QStringList &strl, const QString &letters, bool uniqPerWord) {
    t_letter_stat result;
    if(uniqPerWord)
        std::for_each(strl.constBegin(), strl.constEnd(), makeContUniqStatSummator(result));
    else
        std::for_each(strl.constBegin(), strl.constEnd(), makeContStatSummator(result));

    foreach(QChar ch, distincter(removeNonLetters(alphabetize(letters))))
        result.remove(ch);

    return result;
}

QStringList Dictionary::scoreToList(const t_scores &scores) {
    QStringList result;
    foreach(const t_score &score, scores)
        result << score.first;
    return result;
}

QChar Dictionary::alphabetize(QChar ch) {
    if(!ch.isLetter())
        return ch;
    const QString &decomp = ch.decomposition(); // remove diacritics signs (umlauts, etc...)
    QChar preLetter = decomp.isEmpty() ? ch : decomp.at(0);
    return preLetter.unicode() == 216 ? QLatin1Char('O') : preLetter; // fix for Latin Capital letter O with stroke
}

QString Dictionary::alphabetize(const QString &str) {
    const int str_size = str.size();
    QString result(str_size, QChar());
    for(int i = 0; i < str_size; ++i)
        result[i] = alphabetize(str.at(i));
    return result;
}

int Dictionary::calcCost(QChar ch) {
    char cha = Dictionary::alphabetize(ch).toLatin1();
    return cha < 'A' || cha > 'Z' ? 0 : charCost[cha - 'A'];
}

Dictionary::Dictionary(QObject *parent)
    : QObject(parent), aggWatcher(0) {
    connect(this, SIGNAL(dictReady()), SLOT(makeIndex()));
}
