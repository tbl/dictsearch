/*
  DictSearch
Copyright (C) 2012 Marat Buharov

This file is part of DictSearch.

DictSearch is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, version 3 of the License.

DictSearch is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with DictSearch.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "about.h"
#include "ui_about.h"
#include "product_version.h"
#include <QTextStream>
#include <QFile>

About::About(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::About) {
    ui->setupUi(this);
    ui->lDesc->setText(ui->lDesc->text().arg(QLatin1String(ABOUT_VERSION)));

    QFile lic(QLatin1String(":/licenses.html"));
    if(lic.open(QFile::ReadOnly)) {
        ui->licenseBrowser->setHtml(QTextStream(&lic).readAll());
        lic.close();
    }
}

About::~About() {
    delete ui;
}
