/*
  DictSearch
Copyright (C) 2012 Marat Buharov

This file is part of DictSearch.

DictSearch is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, version 3 of the License.

DictSearch is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with DictSearch.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "mainwidget.h"
#include "ui_mainwidget.h"
#include "dictionary.h"
#include <QShortcut>
#include "stattablemodel.h"
#include "uppervalidator.h"
#include "scoretablemodel.h"
#include "colorerproxymodel.h"
#include <QStateMachine>
#include <QState>
#include "splash.h"
#include "costmodifierfactory.h"
#include <QMenu>
#include <QMessageBox>
#include <QContextMenuEvent>
#include <QFile>
#include <QTextStream>
#include "about.h"

MainWidget::MainWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::MainWidget), d(new Dictionary(this)), mWBL(new ScoreTableModel(d)),
    mWBLP(new ScoreTableModel(d)), mST(new StatTableModel(mWBLP)),
    mMultPos(-1), mMultVal(CostModifierBase::IDENT), cmf(new CostModifierFactory(this)),
    aCommonDict(0), aHWFDict(0), agDictGrp(new QActionGroup(this)) {

    ui->setupUi(this);

    pbMult[0] = ui->pbMult1;
    pbMult[1] = ui->pbMult2;
    pbMult[2] = ui->pbMult3;
    pbMult[3] = ui->pbMult4;
    pbMult[4] = ui->pbMult5;
    pbMult[5] = ui->pbMult6;
    pbMult[6] = ui->pbMult7;
    pbMult[7] = ui->pbMult8;

    QHeaderView *hv = ui->tvLetterStats->verticalHeader();
    hv->setDefaultSectionSize(hv->minimumSectionSize());
    hv = ui->tvWordByLetters->verticalHeader();
    hv->setDefaultSectionSize(hv->minimumSectionSize());
    hv = ui->tvWordByLettersPos->verticalHeader();
    hv->setDefaultSectionSize(hv->minimumSectionSize());

    UpperValidator *vUpper = new UpperValidator(this);
    ui->leWordByLetters->setValidator(vUpper);
    ui->leWordByLettersPos->setValidator(vUpper);
    ui->leWordByLettersPosNW->setValidator(vUpper);

    QKeySequence kEnter(Qt::Key_Enter);
    QKeySequence kReturn(Qt::Key_Return);
    QKeySequence kEscape(Qt::Key_Escape);

    QShortcut *sEnter = new QShortcut(kEnter, this);
    QShortcut *sReturn = new QShortcut(kReturn, this);
    QShortcut *sEscape = new QShortcut(kEscape, this);
    QShortcut *sF1 = new QShortcut(QKeySequence(Qt::Key_F1), this);
    QShortcut *sF2 = new QShortcut(QKeySequence(Qt::Key_F2), this);
    QShortcut *sUp = new QShortcut(QKeySequence(Qt::Key_Up), this);
    QShortcut *sDown = new QShortcut(QKeySequence(Qt::Key_Down), this);
    for(int i = 0; i < 8; ++i) {
        pbMult[i]->setText(*cmf->cm(CostModifierBase::IDENT));
        QShortcut *s = new QShortcut(QKeySequence(Qt::Key_1 + i), this);
        connect(s, SIGNAL(activated()), pbMult[i], SLOT(animateClick()));
    }

    connect(sF1, SIGNAL(activated()), SLOT(setActiveWBL()));
    connect(sF2, SIGNAL(activated()), SLOT(setActiveWBLP()));
    connect(sUp, SIGNAL(activated()), SIGNAL(focusUp()));
    connect(sDown, SIGNAL(activated()), SLOT(focusDown()));
    connect(this, SIGNAL(focusUp()), SLOT(resetFocus()));
    connect(ui->leWordByLetters, SIGNAL(key_Clicked(int)), SLOT(key_Clicked(int)));

    connect(sEnter, SIGNAL(activated()), ui->pbSearch, SLOT(animateClick()));
    connect(sReturn, SIGNAL(activated()), ui->pbSearch, SLOT(animateClick()));
    connect(sEscape, SIGNAL(activated()), ui->pbClear, SLOT(animateClick()));

    connect(this, SIGNAL(searchWordsByLetter(QString,int,const CostModifierBase*)), mWBL, SLOT(searchWordsByLetters(QString,int,const CostModifierBase*)));
    connect(this, SIGNAL(searchWordsByLetterPos(QString,QString)),
            mWBLP, SLOT(searchWordsByLettersPos(QString,QString)));
    connect(mWBLP, SIGNAL(sizeChanged(int)), ui->leWBLPCount, SLOT(setInt(int)));

    ui->pbSearch->setToolTip(tr("Search in dictionary (%1 | %2)").arg(kEnter.toString(), kReturn.toString()));
    ui->pbClear->setToolTip(tr("Clear fields (%1)").arg(kEscape.toString()));

    ColorerProxyModel *mcWBL = new ColorerProxyModel(this);
    ColorerProxyModel *mcWBLP = new ColorerProxyModel(this);
    ColorerProxyModel *mcST = new ColorerProxyModel(this);

    mcWBL->setSourceModel(mWBL);
    mcWBLP->setSourceModel(mWBLP);
    mcST->setSourceModel(mST);

    ui->tvWordByLetters->setModel(mcWBL);
    ui->tvWordByLettersPos->setModel(mcWBLP);
    ui->tvLetterStats->setModel(mcST);

    ui->tvWordByLetters->sortByColumn(1, Qt::DescendingOrder);
    ui->tvWordByLettersPos->sortByColumn(1, Qt::DescendingOrder);
    ui->tvLetterStats->sortByColumn(2, Qt::DescendingOrder);

    ui->splitterWBLP->setStretchFactor(0, 2);
    ui->splitterWBLP->setStretchFactor(1, 3);
    ui->vlWBLP->setStretchFactor(ui->splitterWBLP, 1);

    resetFocus();

    initMultSelector();

    aCommonDict = new QAction(tr("Common"), this);
    aHWFDict = new QAction(tr("Hanging with friends"), this);
    aCommonDict->setData(QString::fromLatin1(":/dictionary/"));
    aHWFDict->setData(QString::fromLatin1(":/hwf_dictionary/"));
    aCommonDict->setCheckable(true);
    aHWFDict->setCheckable(true);
    agDictGrp->addAction(aCommonDict);
    agDictGrp->addAction(aHWFDict);
    aHWFDict->setChecked(true);
    load(aHWFDict->data().toString());
    connect(aCommonDict, SIGNAL(toggled(bool)), SLOT(load(bool)));
    connect(aHWFDict, SIGNAL(toggled(bool)), SLOT(load(bool)));
}

void MainWidget::load(bool toggled) {
    if(!toggled)
        return;
    QAction *act = qobject_cast<QAction *>(sender());
    if(act != 0)
        load(act->data().toString());
}

void MainWidget::load(const QString &str) {
    Splash *splash = new Splash(this);

    connect(d, SIGNAL(indexReady()), splash, SLOT(loadReady()));
    connect(d, SIGNAL(wordCountChanged(int)), splash, SLOT(setWordCount(int)));
    connect(d, SIGNAL(preCopyDict()), splash, SLOT(almostFinish()));

    splash->show();
    qApp->processEvents();

    d->load(str);
}

void MainWidget::initMultSelector() {
    QStateMachine *multSelector = new QStateMachine(this);
    QState *sEmpty = new QState();
    QState **ss[8];

    for(int i = 0; i < 8; ++i) {
        ss[i] = new QState*[cmf->size() - 1];
        ss[i][0] = new QState();
        for(int j = 0; j < cmf->size() - 2; ++j) {
            ss[i][j + 1] = new QState();
            ss[i][j]->addTransition(pbMult[i], SIGNAL(clicked()), ss[i][j + 1]);
            ss[i][j]->addTransition(this, SIGNAL(restartMultSelector()), sEmpty);
        }
        sEmpty->addTransition(pbMult[i], SIGNAL(clicked()), ss[i][0]);
        ss[i][3]->addTransition(pbMult[i], SIGNAL(clicked()), sEmpty);
        ss[i][3]->addTransition(this, SIGNAL(restartMultSelector()), sEmpty);
    }

    for(int i = 0; i < 8; ++i)
        for(int ii = 0; ii < 8; ++ii)
            if(ii != i)
                for(int j = 0; j < cmf->size() - 1; ++j)
                    ss[i][j]->addTransition(pbMult[ii], SIGNAL(clicked()), ss[ii][0]);

    multSelector->addState(sEmpty);
    for(int i = 0; i < 8; ++i)
        for(int j = 0; j < cmf->size() - 1; ++j)
            multSelector->addState(ss[i][j]);

    multSelector->setInitialState(sEmpty);

    sEmpty->assignProperty(this, "multPos", -1);
    sEmpty->assignProperty(this, "multVal", cmf->cm(CostModifierBase::IDENT)->enumStr());
    connect(sEmpty, SIGNAL(propertiesAssigned()), SLOT(multStateRefresh()));

    for(int i = 0; i < 8; ++i) {
        for(int j = 0; j < cmf->size() - 1; ++j) {
            ss[i][j]->assignProperty(this, "multPos", i);
            ss[i][j]->assignProperty(this, "multVal", cmf->cm(static_cast<t_id>(j + 1))->enumStr());
            connect(ss[i][j], SIGNAL(propertiesAssigned()), SLOT(multStateRefresh()));
        }
        delete[] ss[i];
    }

    multSelector->start();
}

MainWidget::~MainWidget() {
    delete ui;
}

void MainWidget::searchWordsByLetter() {
    emit searchWordsByLetter(ui->leWordByLetters->text(), multPos(), cmf->cm(multVal()));
    ui->tvWordByLetters->sortByColumn(1, Qt::DescendingOrder);
}

void MainWidget::searchWordsByLetterPos() {
    emit searchWordsByLetterPos(ui->leWordByLettersPos->text(), ui->leWordByLettersPosNW->text());
    ui->tvWordByLettersPos->sortByColumn(1, Qt::DescendingOrder);
    ui->tvLetterStats->sortByColumn(2, Qt::DescendingOrder);
}

void MainWidget::searchWords() {
    QWidget *currentTab = getCurrentTab();
    if(currentTab == ui->tabWordByLetters)
        return searchWordsByLetter();
    if(currentTab == ui->tabWordByLettersPos)
        return searchWordsByLetterPos();
}

void MainWidget::clearFields() {
    QWidget *currentTab = getCurrentTab();
    if(currentTab == ui->tabWordByLetters)
        clearTabWordByLetters();
    else if(currentTab == ui->tabWordByLettersPos)
        clearTabWordByLettersPos();
    resetFocus(currentTab);
}

void MainWidget::clearTabWordByLetters() {
    ui->leWordByLetters->clear();
    mWBL->clear();
    emit restartMultSelector();
}

void MainWidget::clearTabWordByLettersPos() {
    ui->leWordByLettersPos->clear();
    ui->leWordByLettersPosNW->clear();
    mWBLP->clear();
}

QWidget *MainWidget::getCurrentTab() const {
    return ui->tabsSearch->currentWidget();
}

void MainWidget::setActiveWBL() {
    ui->tabsSearch->setCurrentWidget(ui->tabWordByLetters);
    resetFocus();
}

void MainWidget::setActiveWBLP() {
    ui->tabsSearch->setCurrentWidget(ui->tabWordByLettersPos);
    resetFocus();
}

void MainWidget::resetFocus(QObject *parent) {
    if(parent == ui->tabWordByLetters)
        return ui->leWordByLetters->setFocus();
    if(parent == ui->tabWordByLettersPos)
        return ui->leWordByLettersPos->setFocus();
}

void MainWidget::focusDown() {
    QObject *currentTab = getCurrentTab();
    if(currentTab == ui->tabWordByLetters)
        return resetFocus(currentTab);
    if(currentTab == ui->tabWordByLettersPos)
        return ui->leWordByLettersPosNW->setFocus();
}

void MainWidget::multStateRefresh() {
    for(int i = 0; i < 8; ++i)
        pbMult[i]->setText(*cmf->cm(i == multPos() ? multVal() : CostModifierBase::IDENT));
}

void MainWidget::key_Clicked(int i) {
    if(i >= 0 && i < 8)
        pbMult[i]->animateClick();
}

void MainWidget::contextMenuEvent(QContextMenuEvent *event) {
    QMenu menu(this);
    QMenu dict(tr("Dictionary"), this);
    menu.addMenu(&dict);
    dict.addAction(aCommonDict);
    dict.addAction(aHWFDict);
    menu.addSeparator();
    menu.addAction(tr("About"), this, SLOT(about()));
    menu.addAction(tr("About Qt"), this, SLOT(aboutQt()));
    menu.exec(event->globalPos());
}

void MainWidget::about() {
    About dlg(this);
    dlg.exec();
}

void MainWidget::aboutQt() {
    QMessageBox::aboutQt(this);
}
