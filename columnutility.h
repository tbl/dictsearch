/*
  DictSearch
Copyright (C) 2012 Marat Buharov

This file is part of DictSearch.

DictSearch is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, version 3 of the License.

DictSearch is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with DictSearch.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef COLUMNUTILITY_H
#define COLUMNUTILITY_H

#include <QString>
#include <QPair>
#include <QHash>

namespace CompareNS {

template<class Compare> class Proxy;

}

template<class T>
bool comparator(const T &t1, const T &t2) Q_REQUIRED_RESULT;

template<>
bool comparator(const QChar &t1, const QChar &t2) Q_REQUIRED_RESULT;

template<class T1, class T2>
bool comparator(const T1 &a1, const T1 &b1,
                const T2 &a2, const T2 &b2) Q_REQUIRED_RESULT;

template<class T1, class T2, class T3>
bool comparator(const T1 &a1, const T1 &b1,
                const T2 &a2, const T2 &b2,
                const T3 &a3, const T3 &b3) Q_REQUIRED_RESULT;

namespace CompareNS {

template<class Compare>
Proxy<Compare> makeProxy(const Compare &cmp) Q_REQUIRED_RESULT;

template<class Compare>
class Proxy {
    const Compare &cmp;
public:
    Proxy(const Compare &cmp) : cmp(cmp) {}
    template<class T>
    bool operator()(const T &r1, const T &r2) const Q_REQUIRED_RESULT;
};

template<class T>
class ComparerBase {
public:
    virtual bool operator()(const T &t1, const T &t2) const Q_REQUIRED_RESULT = 0;
    virtual ~ComparerBase() {}
};

template<class T, Qt::SortOrder>
class Comparer : public ComparerBase<T> {
};

template<class T, class ComparerAscendingOrder, class ComparerDescendingOrder>
class ComparerSelector {
    QHash<Qt::SortOrder, const ComparerBase<T> *> cont;
    template<Qt::SortOrder SortOrder>
    void insert(const Comparer<T, SortOrder> *wlc) { cont.insert(SortOrder, wlc); }

public:
    explicit ComparerSelector() {
        insert(new ComparerAscendingOrder());
        insert(new ComparerDescendingOrder());
    }

    const ComparerBase<T> &get(Qt::SortOrder so) const Q_REQUIRED_RESULT { return *cont.value(so, 0); }

    ~ComparerSelector() {
        foreach(const ComparerBase<T> *wlc, cont)
            delete wlc;
    }
};

}

template<class T>
bool comparator(const T &t1, const T &t2) {
    return t1 > t2;
}

template<class T1, class T2>
bool comparator(const T1 &a1, const T1 &b1,
                const T2 &a2, const T2 &b2) {
    return a1 == b1 ? comparator(a2, b2) : comparator(a1, b1);
}

template<class T1, class T2, class T3>
bool comparator(const T1 &a1, const T1 &b1,
                const T2 &a2, const T2 &b2,
                const T3 &a3, const T3 &b3) {
    return a1 == b1 ? comparator(a2, b2, a3, b3) : comparator(a1, b1);
}

namespace CompareNS {

template<class Compare>
template<class T>
bool Proxy<Compare>::operator()(const T &r1, const T &r2) const { return cmp(r1, r2); }

template<class Compare>
Proxy<Compare> makeProxy(const Compare &cmp) {
    return cmp;
}

}

template<class T, class Compare>
void orderer(QList<T> &l, const Compare &c, Qt::SortOrder order) {
    if(order == Qt::AscendingOrder || order == Qt::DescendingOrder)
        qSort(l.begin(), l.end(), CompareNS::makeProxy(c.get(order)));
}

#endif // COLUMNUTILITY_H
