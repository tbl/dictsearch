/*
  DictSearch
Copyright (C) 2012 Marat Buharov

This file is part of DictSearch.

DictSearch is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, version 3 of the License.

DictSearch is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with DictSearch.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef SCORETABLECOLUMNFACTORY_H
#define SCORETABLECOLUMNFACTORY_H

#include <QObject>
#include <QHash>
#include "scoretablecolumn.h"

class ScoreTableColumnFactory : public QObject {
    Q_OBJECT
public:
    explicit ScoreTableColumnFactory(QObject *parent = 0);
    const ScoreTableColumnBase *cm(int id) const Q_REQUIRED_RESULT { return cont.value(id, 0); }
    bool contains(int id) const Q_REQUIRED_RESULT { return cont.contains(id); }
    int getMaxCol() const Q_REQUIRED_RESULT { return maxCol + 1; }
    ~ScoreTableColumnFactory();

private:
    template<int ID>
    void insert(const ScoreTableColumn<ID> *c);
    typedef QHash<int, const ScoreTableColumnBase *> t_cont;
    t_cont cont;
    int maxCol;

    Q_DISABLE_COPY(ScoreTableColumnFactory)
};

#endif // SCORETABLECOLUMNFACTORY_H
