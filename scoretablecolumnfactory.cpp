/*
  DictSearch
Copyright (C) 2012 Marat Buharov

This file is part of DictSearch.

DictSearch is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, version 3 of the License.

DictSearch is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with DictSearch.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "scoretablecolumnfactory.h"
#include "scoretablecolumnscore.h"
#include "scoretablecolumnword.h"

ScoreTableColumnFactory::ScoreTableColumnFactory(QObject *parent) :
    QObject(parent), maxCol(0) {
    insert(new ScoreTableColumn<Word>());
    insert(new ScoreTableColumn<Score>());
}

ScoreTableColumnFactory::~ScoreTableColumnFactory() {
    foreach(const ScoreTableColumnBase *cm, cont)
        delete cm;
}

template<int ID>
void ScoreTableColumnFactory::insert(const ScoreTableColumn<ID> *c) {
    cont.insert(ID, c);
    maxCol = qMax(maxCol, ID);
}
