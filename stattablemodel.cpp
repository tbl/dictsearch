/*
  DictSearch
Copyright (C) 2012 Marat Buharov

This file is part of DictSearch.

DictSearch is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, version 3 of the License.

DictSearch is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with DictSearch.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "stattablemodel.h"
#include "scoretablemodel.h"
#include <QStringList>
#include "dictionary.h"
#include "columnutility.h"
#include <QSet>

QVariant StatTableModel::data(const QModelIndex &index, int role) const {
    int row = index.row();
    int col = index.column();

    return role == Qt::DisplayRole
            && row >= 0
            && index.isValid()
            && row < mLetterStat.size()
            && stcf->contains(col)
            ? stcf->cm(col)->data(mLetterStat.at(row))
            : QVariant();
}

QVariant StatTableModel::headerData(int section, Qt::Orientation orientation, int role) const {
    return role == Qt::DisplayRole
            && orientation == Qt::Horizontal
            && stcf->contains(section)
            ? stcf->cm(section)->headerData()
            : QVariant();
}

StatTableModel::StatTableModel(ScoreTableModel *parentModel)
    : QAbstractTableModel(parentModel), mLetterStat(), pModel(*parentModel), stcf(new StatTableColumnFactory(this)) {
    connect(parentModel, SIGNAL(newData()), SLOT(refreshModel()));
    connect(parentModel, SIGNAL(emptyData()), SLOT(clear()));
}

void StatTableModel::refreshModel() {
    clear();
    Dictionary::t_letter_stat ls = Dictionary::letterStats(Dictionary::scoreToList(pModel.scores()), pModel.letters());
    emit beginResetModel();
    for(Dictionary::t_letter_stat::const_iterator it = ls.constBegin(); it != ls.constEnd(); ++it)
        mLetterStat.append(qMakePair(it.key(), it.value()));
    emit endResetModel();
}

void StatTableModel::clear() {
    emit beginResetModel();
    mLetterStat.clear();
    emit endResetModel();
}

void StatTableModel::sort(int column, Qt::SortOrder order) {
    if(!stcf->contains(column))
        return;

    emit layoutAboutToBeChanged();
    stcf->cm(column)->order(mLetterStat, order);
    emit layoutChanged();
}
