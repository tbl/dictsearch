/*
  DictSearch
Copyright (C) 2012 Marat Buharov

This file is part of DictSearch.

DictSearch is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, version 3 of the License.

DictSearch is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with DictSearch.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef MAINWIDGET_H
#define MAINWIDGET_H

#include <QWidget>
#include "costmodifier.h"

class Dictionary;
class StatTableModel;
class ScoreTableModel;
class QStateMachine;
class QPushButton;
class CostModifierFactory;
class QAction;
class QActionGroup;

namespace Ui {
class MainWidget;
}

class MainWidget : public QWidget {
    Q_OBJECT
    Q_PROPERTY(int multPos READ multPos WRITE setMultPos)
    Q_PROPERTY(t_id multVal READ multVal WRITE setMultVal)
    Q_ENUMS(t_id)

public:
#ifdef Q_MOC_RUN
    enum t_id { IDENT = CostModifierBase::IDENT,
                DL = CostModifierBase::DL,
                TL = CostModifierBase::TL,
                DW = CostModifierBase::DW,
                TW = CostModifierBase::TW };
#else
    typedef CostModifierBase::t_id t_id;
    enum { IDENT = CostModifierBase::IDENT,
           DL = CostModifierBase::DL,
           TL = CostModifierBase::TL,
           DW = CostModifierBase::DW,
           TW = CostModifierBase::TW };
#endif
    explicit MainWidget(QWidget *parent = 0);
    ~MainWidget();
    int multPos() const Q_REQUIRED_RESULT { return mMultPos; }
    void setMultPos(int i) { mMultPos = i; }
    t_id multVal() const Q_REQUIRED_RESULT { return mMultVal; }
    void setMultVal(CostModifierBase::t_id i) { mMultVal = i; }

public slots:
    void searchWordsByLetter();
    void searchWordsByLetterPos();
    void searchWords();
    void clearFields();
    void setActiveWBL();
    void setActiveWBLP();
    void focusDown();
    void resetFocus() { resetFocus(getCurrentTab()); }
    void multStateRefresh();
    void key_Clicked(int i);
    void about();
    void aboutQt();
    void load(bool toggled);

signals:
    void searchWordsByLetter(const QString &, int, const CostModifierBase *);
    void searchWordsByLetterPos(const QString &, const QString &);
    void focusUp();
    void restartMultSelector();

protected:
    void contextMenuEvent(QContextMenuEvent *event);

private:
    QWidget *getCurrentTab() const Q_REQUIRED_RESULT;
    void resetFocus(QObject *parent);
    void clearTabWordByLettersPos();
    void clearTabWordByLetters();
    void initMultSelector();
    void load(const QString &str);

    Ui::MainWidget *ui;
    Dictionary *d;
    ScoreTableModel *mWBL;
    ScoreTableModel *mWBLP;
    StatTableModel *mST;
    int mMultPos;
    CostModifierBase::t_id mMultVal;
    const CostModifierFactory *cmf;
    QAction *aCommonDict;
    QAction *aHWFDict;
    QActionGroup *agDictGrp;

    QPushButton *pbMult[8];

    Q_DISABLE_COPY(MainWidget)
};

#endif // MAINWIDGET_H
