#
#   DictSearch
# Copyright (C) 2012 Marat Buharov
#
# This file is part of DictSearch.
#
# DictSearch is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, version 3 of the License.
#
# DictSearch is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with DictSearch.  If not, see <http://www.gnu.org/licenses/>.
#

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT             += widgets
greaterThan(QT_MAJOR_VERSION, 4): CONFIG         += c++11
lessThan(QT_MAJOR_VERSION, 5):    QMAKE_CXXFLAGS += -std=c++0x

QMAKE_CXXFLAGS += -fvisibility-inlines-hidden -fvisibility=hidden
QMAKE_CXXFLAGS_DEBUG += -Og -ffast-math
QMAKE_CXXFLAGS_RELEASE += -Ofast -fomit-frame-pointer

TARGET = DictSearch
TEMPLATE = app
NW_VERSION = 0,0,1,1
NW_VERSION_DOT = 0.0.1.1

SOURCES += main.cpp\
        mainwidget.cpp \
    dictionary.cpp \
    stattablemodel.cpp \
    uppervalidator.cpp \
    scoretablemodel.cpp \
    intlineedit.cpp \
    colorerproxymodel.cpp \
    letteredit.cpp \
    aggregatewatcher.cpp \
    splash.cpp \
    costmodifier.cpp \
    costmodifierdl.cpp \
    costmodifiertl.cpp \
    costmodifierdw.cpp \
    costmodifiertw.cpp \
    costmodifierfactory.cpp \
    scoretablecolumn.cpp \
    scoretablecolumnword.cpp \
    scoretablecolumnscore.cpp \
    scoretablecolumnfactory.cpp \
    columnutility.cpp \
    costmodifierident.cpp \
    tablecolumn.cpp \
    about.cpp \
    stattablecolumn.cpp \
    stattablecolumnfactory.cpp \
    stattablecolumnchar.cpp \
    stattablecolumnscore.cpp \
    stattablecolumncount.cpp

HEADERS  += mainwidget.h \
    dictionary.h \
    stattablemodel.h \
    uppervalidator.h \
    scoretablemodel.h \
    intlineedit.h \
    colorerproxymodel.h \
    letteredit.h \
    aggregatewatcher.h \
    splash.h \
    costmodifier.h \
    costmodifierdl.h \
    costmodifiertl.h \
    costmodifierdw.h \
    costmodifiertw.h \
    costmodifierfactory.h \
    scoretablecolumn.h \
    scoretablecolumnword.h \
    scoretablecolumnscore.h \
    scoretablecolumnfactory.h \
    columnutility.h \
    costmodifierident.h \
    tablecolumn.h \
    product_version.h \
    about.h \
    stattablecolumn.h \
    stattablecolumnfactory.h \
    stattablecolumnchar.h \
    stattablecolumnscore.h \
    stattablecolumncount.h

FORMS    += \
    mainwidget.ui \
    about.ui

TRANSLATIONS += dictsearch_ru.ts

OTHER_FILES += \
    dictsearch_ru.ts \
    splash.png \
    dictsearch.rc \
    dictsearch.ico

DEFINES += \
    QT_NO_CAST_FROM_ASCII \
    QT_NO_CAST_TO_ASCII

RESOURCES += \
    dictionary.qrc \
    others.qrc \
    licenses.qrc \
    hwf_dictionary.qrc

RC_FILE += \
    dictsearch.rc

DEFINES += \
    "VERSION_RC=$${NW_VERSION}" \
    "EXEC_NAME=$${TARGET}" \
    "VERSION_RC_DOT=$${NW_VERSION_DOT}"
