/*
  DictSearch
Copyright (C) 2012 Marat Buharov

This file is part of DictSearch.

DictSearch is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, version 3 of the License.

DictSearch is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with DictSearch.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "costmodifierfactory.h"
#include "costmodifier.h"
#include "costmodifierident.h"
#include "costmodifierdl.h"
#include "costmodifiertl.h"
#include "costmodifierdw.h"
#include "costmodifiertw.h"

CostModifierFactory::CostModifierFactory(QObject *parent)
    : QObject(parent) {
    insert(new CostModifier<CostModifierBase::IDENT>());
    insert(new CostModifier<CostModifierBase::DL>());
    insert(new CostModifier<CostModifierBase::TL>());
    insert(new CostModifier<CostModifierBase::DW>());
    insert(new CostModifier<CostModifierBase::TW>());
}

CostModifierFactory::~CostModifierFactory() {
    foreach(const CostModifierBase *cm, cont)
        delete cm;
}
