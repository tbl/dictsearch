/*
  DictSearch
Copyright (C) 2012 Marat Buharov

This file is part of DictSearch.

DictSearch is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, version 3 of the License.

DictSearch is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with DictSearch.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "splash.h"

Splash::Splash(QWidget *main)
    : QSplashScreen(QPixmap(QLatin1String(":/splash.png")), Qt::WindowStaysOnTopHint), pMain(main) {
    setAttribute(Qt::WA_DeleteOnClose);
    showMessage(tr("Loading..."), msgAlign);
}

const Qt::Alignment Splash::msgAlign = Qt::AlignHCenter | Qt::AlignBottom;

void Splash::loadReady() {
    showMessage(tr("Finish"), msgAlign);
    finish(pMain);
}
