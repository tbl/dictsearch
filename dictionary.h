/*
  DictSearch
Copyright (C) 2012 Marat Buharov

This file is part of DictSearch.

DictSearch is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, version 3 of the License.

DictSearch is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with DictSearch.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef DICTIONARY_H
#define DICTIONARY_H

#include <QObject>
#include <QStringList>
#include <QHash>
#include <QMultiHash>
#include <QBitArray>
#include <QSet>

class AggregateWatcher;
class CostModifierBase;

class Dictionary : public QObject {
    Q_OBJECT

public:
    typedef QStringList t_container;
    typedef QHash<char, QBitArray> t_bitmap;
    typedef QHash<QChar, t_bitmap> t_bitindex;
    typedef QMultiHash<QString, int> t_sorted_cont;
    typedef QHash<QChar, QBitArray> t_letter_map;
    typedef QHash<QChar, int> t_letter_stat;
    typedef QPair<QString, int> t_score;
    typedef QList<t_score> t_scores;

    explicit Dictionary(QObject *parent = 0);
    t_scores getWordsByLetters(
            const QString &letters, int maxCount, int minWordLen = 1, int maxWordLen = 0, int multPos = -1, const CostModifierBase *cm = 0) const Q_REQUIRED_RESULT;
    t_scores getWordsByPosLetters(
            const QString &letters, const QString &notInWord = QString(),
            int maxCount = 0, bool uniqOpen = true, bool noVowelsAtLast = true) const Q_REQUIRED_RESULT;
    static t_letter_stat letterStats(
            const QStringList &strl, const QString &letters = QString(), bool uniqPerWord = true) Q_REQUIRED_RESULT;
    static QStringList scoreToList(const t_scores &scores) Q_REQUIRED_RESULT;
    static QChar alphabetize(QChar ch) Q_REQUIRED_RESULT;
    static QString alphabetize(const QString &str) Q_REQUIRED_RESULT;
    static QString formatInt(int i) Q_REQUIRED_RESULT { return QString::fromLatin1("%1").arg(i); }
    static int calcCost(QChar ch) Q_REQUIRED_RESULT;
    void load(const QString &path);

public slots:
    void loadFinished();
    void loadAllFinished();

signals:
    void dictReady();
    void indexReady();
    void wordCountChanged(int);
    void preCopyDict();

private slots:
    void makeIndex();

private:
    typedef QSet<QString> t_uniq_container;

    t_container dict;
    t_bitindex index;
    t_sorted_cont sortedDict;
    t_bitmap lengthMap;
    t_letter_map letterInWord;
    AggregateWatcher *aggWatcher;
    t_uniq_container uniqDict;

    void clearDict();
    void clearIndex();

    Q_DISABLE_COPY(Dictionary)
};

#endif // DICTIONARY_H
