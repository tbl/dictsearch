/*
  DictSearch
Copyright (C) 2012 Marat Buharov

This file is part of DictSearch.

DictSearch is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, version 3 of the License.

DictSearch is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with DictSearch.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef PRODUCT_VERSION_H
#define PRODUCT_VERSION_H

#define MAKESTRING(x) #x
#ifdef _MSC_VER
#define EXPANDMACROx4(y) MAKESTRING(y)
#else

#define VERSIONSTRING(a,b,c,d) MAKESTRING(a) "," MAKESTRING(b) "," MAKESTRING(c) "," MAKESTRING(d)
#define EXPANDMACROx4(y) VERSIONSTRING(y)

#endif

#define EXPANDMACROx1(y) MAKESTRING(y)
#define FILE_VERSION_STRING EXPANDMACROx4(VERSION_RC)
#define JOIN_STRINGS(a,b) a " - " EXPANDMACROx1(b)
#define PRODUCT_VERSION_STRING FILE_VERSION_STRING
#define EXEC_NAME_STRING EXPANDMACROx1(EXEC_NAME) ".exe"
#define ABOUT_VERSION EXPANDMACROx1(VERSION_RC_DOT)

#endif // PRODUCT_VERSION_H
