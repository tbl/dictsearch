/*
  DictSearch
Copyright (C) 2012 Marat Buharov

This file is part of DictSearch.

DictSearch is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, version 3 of the License.

DictSearch is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with DictSearch.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef SCORETABLECOLUMNSCORE_H
#define SCORETABLECOLUMNSCORE_H

#include "scoretablecolumn.h"
#include <QCoreApplication>
#include "dictionary.h"

enum { Score = 1 };

template<>
class ScoreTableColumn<Score> : public ScoreTableColumnBase {
public:
    ScoreTableColumn<Score>() : ScoreTableColumnBase(QCoreApplication::translate("ScoreTableColumnScore", "Score")) {}
    QString data(const QPair<QString, int> &p) const Q_REQUIRED_RESULT { return Dictionary::formatInt(p.second); }
    void order(QList<QPair<QString, int> > &l, Qt::SortOrder order) const;
};

#endif // SCORETABLECOLUMNSCORE_H
